async function fetchData(){
	let result = await fetch("https://jsonplaceholder.typicode.com/todos");
	let json = await result.json();
	console.log(json);
}
fetchData();

fetch("https://jsonplaceholder.typicode.com/todos/1")
.then((response) => response.json())
.then((json) => console.log(json))
.then((json) => console.log('The item "delectus aut autem" on the list has a status of false'));


fetch("https://jsonplaceholder.typicode.com/todos",
{
	method: "POST",
	headers: {
		"Content-Type" : "application/json"
	},
	body : JSON.stringify({
		completed: false,
		id: 201,
		title: "Created To Do List Item",
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

fetch("https://jsonplaceholder.typicode.com/todos/1",
{
	method: "PUT",
	headers: {
		"Content-Type" : "application/json"
	},
	body : JSON.stringify({
		dateCompleted : "Pending",
	    description : "To update the my to do list with a different data structure",
	    id : 1,
	    status : "Pending",
	    title : "Updated To Do List Item",
	    userId : 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

fetch("https://jsonplaceholder.typicode.com/todos/1",
{
	method: "PATCH",
	headers: {
		"Content-Type" : "application/json"
	},
	body : JSON.stringify({
		completed : false,
		dateCompleted : "01/16/2023",
		id : 1,
		status : "Completed",
		title : "delectus aut autem",
		userId : 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "DELETE"
});